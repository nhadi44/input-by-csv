<?php

use App\Http\Controllers\InputCSVController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InputCSVController::class, 'index']);
Route::post('/upload-csv', [InputCSVController::class, 'uploadCSV'])->name('upload-csv');
Route::get('/import-csv', [InputCSVController::class, 'parsCSV'])->name('import-csv');
Route::get('/search', [InputCSVController::class, 'search'])->name('search');
Route::post('/store-data', [InputCSVController::class, 'storeData'])->name('store-data');
