<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestCSVImport;
use App\Models\Contact;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel;

class InputCSVController extends Controller
{
    public function index()
    {
        return view('import-csv');
    }

    public function uploadCSV(Request $request)
    {
        $request->validate([
            'csv_file' => 'required|mimes:csv,txt'
        ]);

        $file = $request->file('csv_file');
        $fileName = 'sample.csv';
        if (Storage::disk('local')->exists($fileName)) {
            Storage::disk('local')->delete($fileName);
        }
        Storage::disk('local')->put($fileName, file_get_contents($file));
        return redirect()->route('import-csv');
    }

    public function parsCSV()
    {
        $headers = ['First Name', 'Last Name', 'Country', 'Postal Code', 'Email'];
        $path = Storage::disk('local')->path('sample.csv');
        $file = fopen($path, 'r');
        $all_data = array();
        while (($data = fgetcsv($file, 200, ";")) !== FALSE) {
            $all_data[] = $data;
        }

        return view('field-import', compact('all_data', 'headers'));
    }

    public function search()
    {

        $path = Storage::disk('local')->path('sample.csv');
        $file = fopen($path, 'r');
        $all_data = array();
        while (($data = fgetcsv($file, 200, ";")) !== FALSE) {
            $all_data[] = $data;
        }
        return response()->json($all_data);
    }

    public function storeData(Request $request)
    {
        try {
            for ($i = 0; $i < count($request->data); $i++) {
                $answers[] = [
                    'value_1' => $request->data[$i][0],
                    'value_2' => $request->data[$i][1],
                    'value_3' => $request->data[$i][2],
                    'value_4' => $request->data[$i][3],
                    'value_5' => $request->data[$i][4],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }
            Contact::insert($answers);
            return back()->with('success', 'Data inserted successfully');
        } catch (\Throwable $th) {
            return response()->json($th);
        }
    }
}
