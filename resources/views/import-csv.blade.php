@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row vh-100 d-flex align-items-center justify-content-center">
            <div class="col-md-6">
                <div class="card ">
                    <div class="card-body p-4">
                        <h2 class="text-center">Import CSV</h2>
                        <form method="POST" action="{{ route('upload-csv') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group mb-3 @error('csv_file') is-invalid @enderror">
                                <label for="csv_file" class="form-label">CSV file to import</label>
                                <input id="csv_file" type="file" class="form-control" name="csv_file" accept=".csv"
                                    required>

                                @error('csv_file')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                            <button class="btn btn-primary w-100">
                                Import CSV
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
