@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <h1>Data Import</h1>
            <form>
                <table class="table">
                    <thead>
                        <tr>
                            @foreach ($all_data[0] as $ket => $value)
                                <th>
                                    <select name="field[{{ $ket }}]">
                                        @foreach ($headers as $key => $val)
                                            <option value="{{ $key }}" {{ $ket == $key ? 'selected' : '' }}>
                                                {{ $val }}
                                            </option>
                                        @endforeach
                                    </select>
                                </th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($all_data as $row)
                            <tr>
                                @foreach ($row as $key => $value)
                                    <td id="{{ $key }}" class="test">
                                        {{ $value }}
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>

                </table>

                <button type="submit" class="btn btn-primary" id="submit">
                    Import Data
                </button>
            </form>
        </div>
    </div>
    <script>
        $('document').ready(function() {
            $.ajax({
                url: '/search',
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    $.each(data[0], function(key, value) {
                        $(`select[name="field[${key}]"]`).change(function() {
                            var table = $('table');
                            var selected = $(this).val();
                            for (var i = 0; i < data.length; i++) {
                                var row = table.find('tr').eq(i + 1);
                                var cell = row.find('td').eq(key);
                                cell.html(data[i][selected]);
                            }
                        });
                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#submit').click(function(e) {
            var data = [];
            var table = $('table tbody tr');
            var firstname = []
            var lastname = ""
            table.each(function() {
                var row = $(this);
                var obj = {};
                row.find('td').each(function() {
                    var cell = $(this);
                    var key = cell.attr('id');
                    var value = cell.html();
                    obj[key] = value;
                });
                data.push(obj);
            });

            $.ajax({
                type: 'POST',
                url: "/store-data",
                data: {
                    data: data,
                },
                success: function(data) {
                    console.log(data);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        });
    </script>
@endsection
